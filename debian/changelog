hotdoc-0.8 (0.8.3v2021dev0co4) apertis; urgency=medium

  * debian/apertis/component: Set to development
  * debian/apertis/gitlab-ci.yml: Drop since we use an external definition

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 01 Mar 2021 08:05:17 +0000

hotdoc-0.8 (0.8.3v2021dev0co3) apertis; urgency=medium

  * d/gbp.conf: Fix spurious escape

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sun, 28 Feb 2021 02:19:28 +0000

hotdoc-0.8 (0.8.3v2021dev0co2) apertis; urgency=medium

  * No need to specific usr/bin and usr/lib as it's now a single package.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Tue, 17 Dec 2019 04:02:23 +0800

hotdoc-0.8 (0.8.3v2021dev0co1) apertis; urgency=medium

  * Rename to hotdoc-0.8. Needed for building Apertis-specific packages.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Mon, 16 Dec 2019 14:29:31 +0800

hotdoc-0.8 (0.8.3-0co1) apertis; urgency=medium

  * Rename to hotdoc-0.8. Needed for building Apertis-specific packages.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Mon, 16 Dec 2019 14:29:31 +0800

hotdoc (0.8.3-0co2) apertis; urgency=medium

  * Build against toposort 1.5

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Wed, 13 Feb 2019 15:00:18 +0100

hotdoc (0.8.3-0co1) 16.12; urgency=medium

  * New upstream version.

 -- Andrew Shadura <andrew.shadura@collabora.co.uk>  Wed, 09 Nov 2016 16:24:48 +0100

hotdoc (0.8.1-0co2) 16.12; urgency=medium

  [ Mathieu Duponchelle ]
  * d/copyright: update to include cmark copyrights (Apertis: T2479).

 -- Andrew Shadura <andrew.shadura@collabora.co.uk>  Mon, 17 Oct 2016 17:41:40 +0200

hotdoc (0.8.1-0co1) 16.12; urgency=medium

  * New upstream release.
  * Drop patches applied upstream.
  * Remove no longer relevant dependencies.
  * Use pybuild dh buildsystem.
  * Remove a binary cmark parser on clean.
  * Add debian/watch.

 -- Andrew Shadura <andrew.shadura@collabora.co.uk>  Sun, 16 Oct 2016 23:31:04 +0200

hotdoc (0.8-0co4) 16.12; urgency=medium

  * Don't fail when non-Unicode locale is in use.

 -- Andrew Shadura <andrew.shadura@collabora.co.uk>  Thu, 13 Oct 2016 17:05:15 +0200

hotdoc (0.8-0co3) 16.12; urgency=medium

  * Actually install usr/{bin,lib} into the hotdoc package.

 -- Andrew Shadura <andrew.shadura@collabora.co.uk>  Thu, 13 Oct 2016 16:16:25 +0200

hotdoc (0.8-0co2) 16.12; urgency=medium

  * Drop Breaks/Replaces for hotdoc-gi-extension (<< 0.8).

 -- Andrew Shadura <andrew.shadura@collabora.co.uk>  Wed, 12 Oct 2016 18:37:11 +0200

hotdoc (0.8-0co1) 16.12; urgency=medium

  * New upstream version.
  * Provide transitional packages for hotdoc << 0.8.
  * Add Breaks/Replaces for old hotdoc packages.
  * Install syntax highlighter files correctly.

 -- Andrew Shadura <andrew.shadura@collabora.co.uk>  Wed, 12 Oct 2016 18:11:08 +0200

hotdoc (0.7.10.5-0co2) 16.12; urgency=medium

  * d/p/0001-cmark-Handle-Unicode-return-values-from-the-include-.patch:
    Fix a cmark crash when handling file includes in Unicode
    (Apertis: T1808)

 -- Philip Withnall <philip.withnall@collabora.co.uk>  Thu, 06 Oct 2016 18:38:00 +0100

hotdoc (0.7.10.5-0co1) 16.09; urgency=medium

  [ Simon McVittie ]
  * debian/gbp.conf: configure for Apertis
  * .arcconfig: add; d/source/options: ignore .arcconfig
  * Wrap and sort build-dependencies (wrap-and-sort -abst)
  * Set Vcs-Git, Vcs-Browser to where the packaging is maintained,
    not where the upstream software is maintained

  [ Mathieu Duponchelle ]
  * New upstream version (Apertis: T2438)

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Tue, 16 Aug 2016 16:29:39 +0100

hotdoc (0.7+repack1-0co2) 16.06; urgency=medium

  * d/control: hotdoc depend on python-ipython instead python-ipython4

 -- Héctor Orón Martínez <hector.oron@collabora.co.uk>  Tue, 12 Jul 2016 16:13:37 +0200

hotdoc (0.7+repack1-0co1) 16.06; urgency=medium

  * Add deault theme into upstream tarball to avoid fetching from internet.

 -- Héctor Orón Martínez <hector.oron@collabora.co.uk>  Fri, 01 Jul 2016 15:02:59 +0200

hotdoc (0.7-0co1) 16.03; urgency=medium

  * New upstream release.

 -- Héctor Orón Martínez <hector.oron@collabora.co.uk>  Fri, 26 Feb 2016 01:09:03 +0100

hotdoc (0.6.9.5-0co1) 16.03; urgency=medium

  * New upstream release
     * d/p/fixup-theme-install.patch: dropped fixed upstream

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Thu, 25 Feb 2016 21:59:48 +0100

hotdoc (0.6.9.3-0co2) 16.03; urgency=medium

  * d/p/fixup-theme-install.patch: fix up theme installation
  * d/rules: remove hotdoc eggs on the clean target

 -- Héctor Orón Martínez <hector.oron@collabora.co.uk>  Wed, 17 Feb 2016 17:56:56 +0100

hotdoc (0.6.9.3-0co1) 16.03; urgency=medium

  * New upstream release
  * d/control: build depend on python-cffi
  * d/p/setup-fixes.py: avoid fetches from the interwebs
  * Include default_theme in upstream source tarball, override binary files.

 -- Héctor Orón Martínez <hector.oron@collabora.co.uk>  Wed, 17 Feb 2016 15:36:36 +0100

hotdoc (0.6.9.2-0co2) 16.03; urgency=medium

  * d/control: add hotdoc depends on python-ipython-genutils and python-path.

 -- Héctor Orón Martínez <hector.oron@collabora.co.uk>  Wed, 17 Feb 2016 12:29:28 +0100

hotdoc (0.6.9.2-0co1) 16.03; urgency=medium

  * New upstream release.
    - Fixes hotdoc used for building.

 -- Héctor Orón Martínez <hector.oron@collabora.co.uk>  Tue, 16 Feb 2016 22:21:53 +0100

hotdoc (0.6.9.1-0co3) 16.03; urgency=medium

  * debian/control: correct depends on ipython packages. (Apertis: T1081)
  * debian/control: add versioned depends on python-commonmark.
  * Refreshed adjust-depends.patch depends on correct ipython4 version.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Tue, 16 Feb 2016 03:41:05 +0800

hotdoc (0.6.9.1-0co2) 16.03; urgency=medium

  * Refreshed adjust-depends.patch: drop git-pylint-commit-hook and
    git-pep8-commit-hook.
  * debian/control: depends on python-yaml and python-cmarkpy.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Fri, 12 Feb 2016 23:17:57 +0800

hotdoc (0.6.9.1-0co1) 16.03; urgency=medium

  * New upstream release. (Apertis: T1055)
  * Refreshed adjust-depends.patch.
  * debian/control: build-deps on libffi-dev and libgit2-dev.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Fri, 12 Feb 2016 00:41:37 +0800

hotdoc (0.6.4LTS-0co1+15.12u1) 15.12; urgency=medium

  * New upstream release. (Apertis: T914)

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Thu, 21 Jan 2016 17:04:08 +0800

hotdoc (0.6.4-0co2) 15.12; urgency=medium

  * d/control: hotdoc depends on python-pkg-resources (Apertis: #621)

 -- Héctor Orón Martínez <hector.oron@collabora.co.uk>  Sun, 13 Dec 2015 13:39:08 +0100

hotdoc (0.6.4-0co1) 15.12; urgency=medium

  * New upstream release
  * debian/control: build-deps libgraphviz-dev.
  * Refreshed adjust-depends.patch.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Sat, 12 Dec 2015 02:43:12 +0800

hotdoc (0.6.2-0co1) 15.12; urgency=medium

  * debian/control: updated runtime depends instead of build-deps.
  * New upstream release.
  * Applied adjust-depends.patch.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Thu, 10 Dec 2015 04:51:16 +0800

hotdoc (0.5.9.5-0co2) 15.12; urgency=medium

  * debian/control: build-deps on python-sqlalchemy,
    python-wheezy.template. (Apertis: T725)

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Fri, 04 Dec 2015 01:50:11 +0800

hotdoc (0.5.9.5-0co1) 15.12; urgency=low

  * Initial release. (Apertis: T725, #531)

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Wed, 21 Jan 2015 02:55:36 +0800
